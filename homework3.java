package homework;

public class homework3 {
	public static void main(String[] args) {
		int n = 4;
		// drow9(n);
		// drow10(n);
		// drow11(n);
		// drow12(n);
		// drow13(n);
		//drow14(n);
		//drow15(n);
		// drow16(n);
		// drow17(n);
	}

	public static int drow9(int number) {

		for (int num = 0; num < number; num++) {
			int num2 = num * 2;
			System.out.println(num2);
		}
		return number;
	}

	public static int drow10(int number2) {

		for (int num = 1; num <= number2; num++) {
			int num2 = num * 2;
			System.out.println(num2);
		}
		return number2;
	}

	public static int drow11(int number3) {
		int k = 1;
		for (int num1 = 1; num1 <= number3; num1++) {
			for (int num2 = 1; num2 <= number3; num2++, k++) {
				System.out.print(k);
			}
			System.out.print("\n");
		}
		System.out.println("");

		return number3;
	}

	public static int drow12(int number4) {
		String[] result = new String[number4];
		int num;
		for (num = 0; num < number4; num++)
			result[num] = "*";
		for (num = 0; num < number4; num++) {
			result[num] = "-";
			for (int num2 = 0; num2 < number4; num2++)
				System.out.print(result[num2]);
			result[num] = "*";

			System.out.println();
		}

		return number4;
	}

	public static int drow13(int number5) {
		String[] result = new String[number5];
		int num;
		for (num = 0; num < number5; num++)
			result[num] = "-";
		for (num = 0; num < number5; num++) {
			result[num] = "*";
			for (int num2 = 0; num2 < number5; num2++)
				System.out.print(result[num2]);
			result[num] = "-";

			System.out.println();
		}
		return number5;
	}

	public static int drow14(int number6) {
		for (int num = 0; num < number6; num++) {
			for (int num2 = 0; num2 <= num; num2++) {
				System.out.print(" *");
			}
			for (int num2 = num; num2 < number6 - 1; num2++) {
				System.out.print(" -");
			}
			System.out.println(" ");
		}
		return number6;
	}
	
	public static int drow15(int number7) {
		for (int num = 1; num <= number7; num++) {
			//System.out.println("num: " + num);
			for (int num2 = num; num2 <= number7; num2++) {
				//System.out.println("num2: " + num2);
				System.out.print(" *");
			}
			for (int num2 = 1; num2 <= num - 1; num2++) {
				System.out.print(" -");
			}
			System.out.println(" ");
		}
		return number7;
	}

	public static int drow16(int number8) {
		for (int num = 0; num < number8; num++) {
			for (int num2 = 0; num2 <= num; num2++) {
				System.out.print("*");
			}
			for (int num2 = num; num2 <= number8 - 2; num2++) {
				System.out.print("-");
			}
			System.out.println("");
		}
		for (int num = 1; num <= number8 - 1; num++) {
			for (int num2 = 1; num2 <= (number8 - num); num2++) {
				System.out.print("*");
			}
			for (int num2 = 1; num2 <= num; num2++) {
				System.out.print("-");
			}
			System.out.println("");
		}

		return number8;
	}

	public static int drow17(int number9) {
		for (int num = 0; num < number9; num++) {
			for (int num2 = 0; num2 <= num; num2++) {
				System.out.print("" + (num + 1));
			}
			for (int num2 = num; num2 <= number9 - 2; num2++) {
				System.out.print("-");
			}
			System.out.println("");
		}
		for (int num = 1; num <= number9 - 1; num++) {
			for (int num2 = 1; num2 <= (number9 - num); num2++) {
				System.out.print("" + (number9 - num));
			}
			for (int num2 = 1; num2 <= num; num2++) {
				System.out.print("-");
			}
			System.out.println("");
		}
		return number9;
	}

}
