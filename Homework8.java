package homework;

public class Homework8 {
	public static void main(String[] args) {
		int num = 3;
		// drow1(num);
		// drow2(num);
		 drow3(num);
		// drow4(num);
		// drow5(num);
		// drow6(num);
		// drow7(num);
		// drow8(num);
	}

	public static int drow1(int number) {
		for (int num1 = 0; num1 < number; num1++) {
			System.out.print("*");
		}
		return number;
	}

	public static int drow2(int number1) {
		for (int num1 = 0; num1 < number1; num1++) {
			for (int num2 = 0; num2 < number1; num2++) {
				System.out.print("*");
			}
			System.out.println("");
		}

		return number1;
	}

	public static int drow3(int number2) {
		for (int num1 = 1; num1 <= number2; num1++) {
			//System.out.print("num : " + num1);
			for (int num2 = 1; num2 <= number2; num2++) {
				System.out.print(num2);
			}
			System.out.println("");
		}

		return number2;
	}

	public static int drow4(int number3) {
		for (int num1 = number3; num1 > 0; num1--) {
			for (int num2 = number3; num2 > 0; num2--) {
				System.out.print(num2);
			}
			System.out.println("");
		}

		return number3;
	}

	public static int drow5(int number4) {
		for (int num1 = 1; num1 <= number4; num1++) {
			for (int num2 = 1; num2 <= number4; num2++) {
				System.out.print(num1);
			}
			System.out.println("");
		}

		return number4;
	}

	public static int drow6(int number5) {
		for (int num1 = number5; num1 >= 1; num1--) {
			for (int num2 = 1; num2 <= number5; num2++) {
				System.out.print(num1);
			}
			System.out.println("");
		}

		return number5;
	}

	public static int drow7(int number6) {
		int k = 1;
		for (int num1 = 1; num1 <= number6; num1++) {
			for (int num2 = 1; num2 <= number6; num2++, k++) {
				System.out.printf("%3d", k);
			}
			System.out.print("\n");
		}
		System.out.println("");

		return number6;
	}

	public static int drow8(double number7) {
		double k = Math.pow(number7, 2);
		for (double num1 = number7; num1 >= 1.0; num1--) {
			for (double num2 = number7; num2 >= 1.0; num2--, k--) {

				System.out.printf("%3d", (int) k);

			}
			System.out.print("\n");
		}
		System.out.println("");
		return (int) number7;
	}

}
