package homework;

public class homework2 {
	private static final String[] String = null;

	public static void main(String[] args) {
		multiplyTable();
	}

	public static void multiplyTable() {
		String[][] table = { { "1", "2", "3" }, { "4", "5", "6" }, { "7", "8", "9" } };
		int[] newTable = new int[table.length];
		// System.out.println(newTable);
		
		for (int row = 0; row < table.length; row++) {
			for (int element1 = 0; element1 < table.length; element1++) {
				newTable[row] = Integer.parseInt(table[row][element1]);
				System.out.printf("%3d",newTable[row] * 2);

			}
			System.out.println("");
		}
	}
}
